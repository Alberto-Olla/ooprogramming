package PhoneBook;

import java.sql.SQLException;
import java.util.HashMap;

public class PhoneBook {
	DBManager db; 
	HashMap<String, Item> pbName;
	HashMap<String, Item> pbNumber;
	
	public PhoneBook() {
		try {
			db = new DBManager();
			db.test();
			pbName = db.load();
		} catch (SQLException e) {
			System.out.println("Something wrong happened while creating DB!");
			System.exit(1);
		}
		
		pbNumber = new HashMap<String, Item>();
		for (String name : pbName.keySet()) {
			Item i = pbName.get(name);
			pbNumber.put(i.getNumber(), i);
		}
	}
	
	public Item getByName(String name) {
		return pbName.get(name);
	}
	
	public Item getByNumber(String number) {
		return pbNumber.get(number);
	}	

}
