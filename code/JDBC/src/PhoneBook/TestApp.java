package PhoneBook;

import java.util.Scanner;


public class TestApp {
	private PhoneBook pb;
	private Scanner scr;

	public TestApp() {
		pb = new PhoneBook();
		scr = new Scanner(System.in);
		mainMenu();
	}

	public void searchByName() {
		String name;
		System.out.println("## PhoneBook (Search by Name) ##");
		System.out.print("Insert name to search ");
		name = scr.next();
		System.out.println(pb.getByName(name));
		System.out.println("");
	};
	
	public void searchByNumber() {
		String number;
		System.out.println("## PhoneBook (Search by Number) ##");
		System.out.print("Insert number to search ");
		number = scr.next();
		System.out.println(pb.getByNumber(number));
		System.out.println("");
	};

	public void mainMenu() {
		int opt;
		do {
			System.out.println("## PhoneBook ##");
			System.out.println("1) Search by Name");
			System.out.println("2) Search by Number");
			System.out.println("3) Quit!");
			opt = scr.nextInt();

			if (opt == 1) searchByName();
			if (opt == 2) searchByNumber();
		} while (opt != 3);

		return;

	}

	public static void main(String[] args) {
		new TestApp();
	}
}
