package PhoneBook;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class DBManager {
	Connection connection;
	Statement statement;

	public DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:phonebook.db");
			statement = connection.createStatement();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void test() throws SQLException {
		try {
			statement.executeQuery("SELECT * FROM numbers LIMIT 1");
		} catch (SQLException e) {
			System.out.println("DB not found! Creating an empty one...");
			statement.executeUpdate("DROP TABLE IF EXISTS numbers");
			statement.executeUpdate("CREATE TABLE numbers (Name VARCHAR(20) UNIQUE, Number VARCHAR(20))");
			statement.executeUpdate("INSERT INTO numbers VALUES('nicola', '111111')");
			statement.executeUpdate("INSERT INTO numbers VALUES('marzia', '222222')");
			statement.executeUpdate("INSERT INTO numbers VALUES('agata',  '333333')");
			statement.executeUpdate("INSERT INTO numbers VALUES('dharma', '444444')");
		}
	}

	public HashMap<String, Item> load() throws SQLException {
		HashMap<String, Item> pb = new HashMap<String, Item>();
		try {
			ResultSet rs = statement.executeQuery("SELECT * FROM numbers");
			while(rs.next()) {
				String name = rs.getString("name");
				String number = rs.getString("number");
				pb.put(name, new Item(name, number));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pb;
	}

}
