package Examples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCConnection {
	protected Statement statement;
	protected Connection connection;

	public JDBCConnection(String db) throws ClassNotFoundException, SQLException {
		if (db.equals("sqlite")) {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection(
					"jdbc:sqlite:sample.db");
		} else if (db.equals("postgres")) {
			Class.forName("org.postgresql.Driver");
			connection = DriverManager.getConnection(
					"jdbc:postgresql:///dbname", "username", "password");
		} else {
			throw new ClassNotFoundException("database not found");
		}
		statement = connection.createStatement();
		statement.setQueryTimeout(30);  
	}

	public void getColumnNames() throws SQLException {
		StringBuilder sb = new StringBuilder();
		System.out.println("- reading database (column names)...");
		ResultSet rs = statement.executeQuery("SELECT * FROM person LIMIT 1 OFFSET 0");

		while (rs.next()){
			for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) { 
				sb.append(rs.getMetaData().getColumnName(i));
				sb.append(",");
			}
		}
		System.out.println(sb.toString());
	}

	public void testInsert() throws SQLException {
		System.out.println("- building database...");
		statement.executeUpdate("DROP TABLE IF EXISTS person");
		statement.executeUpdate("CREATE TABLE person (id INTEGER UNIQUE, name VARCHAR(30), surname VARCHAR(30))");
		try {
			statement.executeUpdate("INSERT INTO person VALUES(1000, 'leo', 'ross')");
			statement.executeUpdate("INSERT INTO person VALUES(1001, 'yui', 'kiuk')");
			statement.executeUpdate("INSERT INTO person VALUES(1002, 'yui', 'kiuk')");
			// statement.executeUpdate("INSERT INTO person VALUES(1002, 'mario', 'rossi')");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void testUpdate() throws SQLException {
		System.out.println("- updating database...");
		statement.executeUpdate("UPDATE person SET name='pippo' WHERE name='leo'");
	}

	public void testSelect() throws SQLException {
		System.out.println("- reading database...");
		ResultSet rs = statement.executeQuery("SELECT * FROM person LIMIT 1000 OFFSET 0");
		while(rs.next()) {
			// read the result set
			System.out.println(rs.getRow() + ": id=" + rs.getInt("id") + ", name=" + rs.getString("name") + ", surname=" + rs.getString("surname"));
		}
	}

	public void closeConnection() {
		if (connection != null)
			try {
				// relevant after 
				// connection.setAutoCommit(false);
				// connection.commit();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	public static void main(String[] args) throws ClassNotFoundException {
		JDBCConnection s = null;

		try{
			s = new JDBCConnection("sqlite");
			s.testInsert();
			s.getColumnNames();
			s.testSelect();
			s.testUpdate();
			s.testSelect();
		} catch(SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (s != null) s.closeConnection();
		}
	}
}
