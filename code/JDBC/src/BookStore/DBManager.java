package BookStore;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DBManager {
	protected Statement statement;
	protected Connection connection;

	public DBManager() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		connection = DriverManager.getConnection("jdbc:sqlite:bookstore.db");
		statement = connection.createStatement();
		statement.setQueryTimeout(30); 
	}

	public void build() throws SQLException {
		statement.executeUpdate("DROP TABLE IF EXISTS person");
		statement.executeUpdate("DROP TABLE IF EXISTS item");
		statement.executeUpdate("DROP TABLE IF EXISTS rent");

		statement.executeUpdate("CREATE TABLE person (" +
				"_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"cf VARCHAR(30) UNIQUE, " +
				"name VARCHAR(30), " +
				"surname VARCHAR(30))");

		statement.executeUpdate("CREATE TABLE item (" +
				"_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"year INTEGER, " +
				"title VARCHAR(30), " +
				"type VARCHAR(10), " +
				"lenght INTEGER)");

		statement.executeUpdate("CREATE TABLE rent (" +
				"_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
				"idperson INTEGER, " +
				"iditem INTEGER, " +
				"begin VARCHAR(10), " +
				"end VARCHAR(10), " + 
				"FOREIGN KEY(idperson) REFERENCES person(_id), " +
				"FOREIGN KEY(iditem) REFERENCES item(_id))");

		statement.executeUpdate("INSERT INTO person (cf, name, surname) VALUES('0001', 'Darrell', 'Abbott')");
		statement.executeUpdate("INSERT INTO person (cf, name, surname) VALUES('0002', 'Nick', 'Drake')");

		statement.executeUpdate("INSERT INTO item (year, title, type, lenght) VALUES(2002, 'Soffocare', 'Book', 170)");
		statement.executeUpdate("INSERT INTO item (year, title, type, lenght) VALUES(2011, 'Moon', 'Dvd', 130)");

		statement.executeUpdate("INSERT INTO rent (idperson, iditem, begin, end) VALUES(1, 1, '15/6/2011', '15/7/2011')");
		statement.executeUpdate("INSERT INTO rent (idperson, iditem, begin, end) VALUES(2, 1, '10/7/2011', '20/7/2011')");
		statement.executeUpdate("INSERT INTO rent (idperson, iditem, begin, end) VALUES(1, 1, '25/8/2011', '14/9/2011')");
		statement.executeUpdate("INSERT INTO rent (idperson, iditem, begin, end) VALUES(2, 2, '10/7/2011', '20/7/2011')");
		statement.executeUpdate("INSERT INTO rent (idperson, iditem, begin, end) VALUES(1, 2, '25/8/2011', '14/10/2011')");
	}

	public ArrayList<Rent> load() throws SQLException {
		HashMap<Integer, Person> personMap = new HashMap<Integer, Person>();
		HashMap<Integer, Item> itemMap = new HashMap<Integer, Item>();
		ArrayList<Rent> r = new ArrayList<Rent>();
		DateTimeFormatter f = DateTimeFormat.forPattern("dd/MM/yyyy");

		ResultSet rs = statement.executeQuery("SELECT * FROM person");
		while(rs.next()) {
			personMap.put(
					rs.getInt("_id"),
					new Person(rs.getString("cf"),
							rs.getString("name"),
							rs.getString("surname")));
		}

		rs = statement.executeQuery("SELECT * FROM item");
		while(rs.next()) {
			if (rs.getString("type") == "book") 
				itemMap.put(
						rs.getInt("_id"),
						new Book(rs.getString("title"),
								rs.getInt("year"),
								rs.getInt("lenght")));
			else
				itemMap.put(
						rs.getInt("_id"),
						new Dvd(rs.getString("title"),
								rs.getInt("year"),
								rs.getInt("lenght")));

		}

		rs = statement.executeQuery("SELECT * FROM rent");
		while(rs.next()) {
			r.add(new Rent(
					itemMap.get(rs.getInt("iditem")),
					personMap.get(rs.getInt("idperson")),
					f.parseDateTime(rs.getString("begin")),
					f.parseDateTime(rs.getString("end"))));
		}

		return r;
	}

	public void closeConnection() {
		if (connection != null) {
			try {
				statement.close();
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

}

