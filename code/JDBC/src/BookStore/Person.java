package BookStore;

/**
 * Person encapsulates persons details
 */
public class Person {
	protected String name;
	protected String surname;
	protected String CF;
	
	public Person(String CF, String name, String surname) {
		this.CF = CF;
		this.name = name;
		this.surname = surname;
	}
	
	public String getName() {
		return name;
	}
	
	public String getSurname() {
		return surname;
	}

	public String getCF() {
		return CF;
	}
	
	public String toString() {
		return name + " " + surname;
	}

}
