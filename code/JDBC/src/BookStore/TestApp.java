package BookStore;

import java.sql.SQLException;
import java.util.ArrayList;
import org.joda.time.Interval;

/**
 * Application main class
 */
public class TestApp {
	
//	Una biblioteca ha la necessita di tenere traccia dei prestiti dei libri e dei DVD 
//	in catalogo. Ad ogni libro o DVD sara' quindi associata una sequenza di prestiti, ad 
//	ognuno dei quali corrispondono la data di inizio prestito, la data di riconsegna, il 
//	nome e cognome dell'utente. Inoltre, per i DVD occorrera' tenere traccia della durata, 
//	mentre per i libri occorrera tenere traccia del numero di pagine. Ad entrambi i tipi di 
//	supporti, infine, bisogna associare il titolo e lʼanno di pubblicazione. Il bibliotecario 
//	potrebbe essere interessato a calcolare il periodo piu' lungo (in giorni) in cui un libro 
//	o un DVD e' rimasto a prestito ad una persona. Occorre, infinte, permettere al bibliotecario 
//	di controllare se nella lista di prestiti relativi ad un libro ad un DVD esistano inconsistenze, 
//	ovvero se un prestito nella lista sia iniziato prima che un altro prestito fosse concluso.
	
	ArrayList<Rent> rents;
	DBManager db;
	
	public TestApp() throws SQLException {
		try {
			db = new DBManager();
			rents = db.load();
		} catch (ClassNotFoundException e) {
			System.out.println("Unable to find libraries...");
		} catch (SQLException e) {
			System.out.println("Unable to find database...");
			System.out.println("Building a new database...");
			db.build();
			rents = db.load();
		} 
		
		for (Rent r : rents) {
			System.out.println(r);
		}
		
		System.out.println("- Longest Rent:");
		System.out.println(getLongestRent());
		
		System.out.println("- Faulty Rents:");
		for (Rent r : getFaults()) {
			System.out.println(r);
		}
	}
	
	private ArrayList<Rent> getFaults() {
		ArrayList<Rent> r = new ArrayList<Rent>();
		for (int i = 0; i < rents.size() - 1; i++) {
			for (int j = i + 1; j < rents.size(); j++) {
				Rent r1 = rents.get(i);
				Rent r2 = rents.get(j);
				Interval i1 = new Interval(r1.getBegin(), r1.getEnd());
				Interval i2 = new Interval(r2.getBegin(), r2.getEnd());
				if (i2.overlaps(i1) && r1.getItem() == r2.getItem()) {
					r.add(rents.get(i));
					r.add(rents.get(j));
				}
			}
		}
		return r;
	}
	
	private Rent getLongestRent() {
		Rent longestRent = rents.get(0);
		
		for (Rent r : rents) {
			if ((r.getEnd().getMillis() - r.getBegin().getMillis()) > 
				(longestRent.getEnd().getMillis() - longestRent.getBegin().getMillis())) {
				longestRent = r;
			}
		}
		return longestRent;
	}
	
	public static void main(String[] args) throws Exception {
		new TestApp();
	}

}
